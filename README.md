# README #

1. bajar la imagen tar
2. importar a docker: docker import dockerphpstorm.tar.gz  (https://docs.docker.com/engine/reference/commandline/import/)
3. ejecutar comando

	docker run --name phpstorm -ti --net host    -v /tmp/.X11-unix:/tmp/.X11-unix  --env="DISPLAY" --volume="$HOME/.Xauthority:/root/.Xauthority:rw"  -v /tmp/compartido:/tmp/compartido -v $HOME:$HOME -d  <id imagen>


### Volumenes ###

* /tmp/compartido
* /tmp/.X11-unix
* $HOME

### Que pasa despues ###

* Despues de ejecutar el comando aparece tormenta de prueba por 30 dias
* Para volver a ejecutarlo las demas veces solo hace falta ejecutar: docker start phpstorm
* Para instalar plugins siempre hace falta cerrar phpstorm y luego ejecutar: docker start phpstorm
* Es un ubuntu con apt, si se quiere instalar algo mas, siempre se puede, incluso desdel el terminal del phpstorm
